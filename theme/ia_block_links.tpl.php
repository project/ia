<?php

/**
 * Theme template for the ia links displayed
 *  in the block.
 *
 * @file: ia_block_links.tpl.php
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 *
 * AVAILABLE VARIABLES:
 *  $links - array of links to post where each link
 *    is an array as follows:
 *    array(
 *      'title' => t('Title'),
 *      'href' => 'path/to/invite/page',
 *    )
 */

// Uncomment the following lines to see the variables
//print_r($links)
?>
<div class="ia_links">
  <ul class="ia_links_list">

    <?php foreach ($links as $link) { ?>
    <li><?php print l($link['title'], $link['href']) ?></li>
    <?php } ?>

  </ul>
  <div class="clear"></div>
</div>
