<?php

/**
 * Theme template for the received invitations for a 
 *  given module.
 *
 * @file: ia_received_invites.tpl.php
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 *
 * AVAILABLE VARIABLES:
 *  $module - the module name
 *  $received_name - the human readable received name
 *  $received - array of received messages keyed as follows:
 *    $received = array(
 *      'invite_id' => array(
 *        'to' => the recipient
 *        'from' => the sender
 *        'withdraw' => withdraw link
 *        'message' => Invite message
 *        'picture' => Picture of the invite subject
 *      ),
 *    );
 */

// Uncomment the following lines to see the variables
//print_r($module)
//print_r($received_name)
//print_r($received)
?>
<div class="<?php print $module ?>_received">
  <h3><?php print $received_name ?></h3>

  <?php foreach ($received as $id => $invite) { ?>
  <div class="ia_received_invite <?php print $module ?>_received_invite" id="<?php print $module ?>_received_invite-<?php print $id ?>">

    <div class="invite_picture"><?php print isset($invite['picture']) ? $invite['picture'] : '' ?></div>
    <div class="invite_from"><?php print isset($invite['from']) ? 'From: ' . $invite['from'] : '' ?></div>
    <div class="invite_message"><?php print isset($invite['message']) ? 'Message: ' . $invite['message'] : '' ?></div>
    <div class="invite_time"><?php print isset($invite['timestamp']) ? 'Sent On: ' . $invite['timestamp'] : '' ?></div>
    <div class="invite_accept"><?php print isset($invite['accept']) ? $invite['accept'] : '' ?></div>
    <div class="invite_decline"><?php print isset($invite['decline']) ? $invite['decline'] : '' ?></div>
    <div class="clear"></div>

  </div>
  <?php } ?>

</div>
