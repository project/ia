<?php

/**
 * Theme template for the sent invitations for a 
 *  given module.
 *
 * @file: ia_sent_invites.tpl.php
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 *
 * AVAILABLE VARIABLES:
 *  $module - the module name
 *  $sent_name - the human readable sent name
 *  $sent - array of sent messages keyed as follows:
 *    $sent = array(
 *      'invite_id' => array(
 *        'to' => the recipient
 *        'from' => the sender
 *        'withdraw' => withdraw link
 *        'message' => Invite message
 *        'picture' => Picture of the invite subject
 *      ),
 *    );
 */

// Uncomment the following lines to see the variables
//print_r($module)
//print_r($sent_name)
//print_r($sent)
?>
<div class="<?php print $module ?>_sent">
  <h3><?php print $sent_name ?></h3>

  <?php foreach ($sent as $id => $invite) { ?>
  <div class="ia_sent_invite <?php print $module ?>_sent_invite" id="<?php print $module ?>_sent_invite-<?php print $id ?>">

    <div class="invite_picture"><?php print isset($invite['picture']) ? $invite['picture'] : '' ?></div>
    <div class="invite_to"><?php print isset($invite['to']) ? 'To: ' . $invite['to'] : '' ?></div>
    <div class="invite_from"><?php print isset($invite['from']) ? 'From: ' . $invite['from'] : '' ?></div>
    <div class="invite_withdraw"><?php print isset($invite['withdraw']) ? 'Cancel: ' . $invite['withdraw'] : '' ?></div>
    <div class="invite_message"><?php print isset($invite['message']) ? 'Message: ' . $invite['message'] : '' ?></div>
    <div class="invite_time"><?php print isset($invite['timestamp']) ? 'Sent on: ' . $invite['timestamp'] : '' ?></div>
    <div class="clear"></div>

  </div>
  <?php } ?>

</div>
