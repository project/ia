<?php

/**
 * Admin callbacks for the ia module.
 *
 * @file: ia.admin.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * ia_settings - return the system settings form
 *  for the ia module.
 */
function ia_settings() {
  $form = array();

  $form['ia_invites_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Invites Per Page'),
    '#default_value' => variable_get('ia_invites_per_page', 10),
    '#description' => t('Enter the number of invites per page to use on the ia page'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

