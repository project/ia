<?php

/**
 * Theme include functions for the ia module.
 *
 * @file: ia.theme.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * Implementation of module_preprocess_ia_sent_invites()
 */
function ia_preprocess_ia_sent_invites(&$variables) {
  // add additional template file overrides
  $variables['template_files'][] = 'ia_sent_invites-' . $variables['module'];

  // update the values in the sent array for a consistent
  // default theme
  $sent =& $variables['sent'];

  foreach ($sent as $k => $invite) {
    // return a themed username if the from field
    // is a user object.
    // NOTE: we're assuming that the invoking modules
    // are only passing a user object.  If it's something
    // else it's up to that module to override the preprocess
    // function
    if (is_object($invite['from'])) {
      $sent[$k]['from'] = theme('username', $invite['from']);
    }

    // same work for the to field
    if (is_object($invite['to'])) {
      $sent[$k]['to'] = theme('username', $invite['to']);

      // and since it's a user object we can add the user's picture too
      if (!isset($sent[$k]['picture'])) {
        $sent[$k]['picture'] = theme('user_picture', $invite['to']);
      }
    }

    // change the withdraw path to a themed cancel link
    // NOTE: again, we're assuming that the module passed
    // a path and not a themed link.  If the module passed
    // something else it's up to that module to override this
    // preprocess.
    if (isset($invite['withdraw'])) {
      $sent[$k]['withdraw'] = l(t('Cancel'), $invite['withdraw'], array('query' => array('destination' => 'ia')));
    }

    // if the invite timestamp is a number we're assuming it's
    // a UNIX timestamp, so convert it to a date.  If it's
    // something else it's up to the module to make sure it
    // gets handed off correctly
    if (is_numeric($invite['timestamp'])) {
      $sent[$k]['timestamp'] = date('M d, Y', $invite['timestamp']);
    }
  }
}

/**
 * Implementation of module_preprocess_ia_received_invites()
 */
function ia_preprocess_ia_received_invites(&$variables) {
  // add additional template file overrides
  $variables['template_files'][] = 'ia_received_invites-' . $variables['module'];

  // update the values in the sent array for a consistent
  // default theme
  $rec =& $variables['received'];

  foreach ($rec as $k => $invite) {
    // return a themed username if the from field
    // is a user object.
    // NOTE: we're assuming that the invoking modules
    // are only passing a user object.  If it's something
    // else it's up to that module to override the preprocess
    // function
    if (is_object($invite['from'])) {
      $rec[$k]['from'] = theme('username', $invite['from']);
    }

    // same work for the to field
    if (is_object($invite['to'])) {
      $rec[$k]['to'] = theme('username', $invite['to']);

      // and since it's a user object we can add the user's picture too
      if (!isset($rec[$k]['picture'])) {
        $rec[$k]['picture'] = theme('user_picture', $invite['to']);
      }
    }

    // change the accept/decline path to a themed cancel link
    // NOTE: again, we're assuming that the module passed
    // a path and not a themed link.  If the module passed
    // something else it's up to that module to override this
    // preprocess.
    if (isset($invite['accept'])) {
      $rec[$k]['accept'] = l(t('Accept'), $invite['accept'], array('query' => array('destination' => 'ia')));
    }
    if (isset($invite['decline'])) {
      $rec[$k]['decline'] = l(t('Decline'), $invite['decline'], array('query' => array('destination' => 'ia')));
    }

    // if the invite timestamp is a number we're assuming it's
    // a UNIX timestamp, so convert it to a date.  If it's
    // something else it's up to the module to make sure it
    // gets handed off correctly
    if (is_numeric($invite['timestamp'])) {
      $rec[$k]['timestamp'] = date('M d, Y', $invite['timestamp']);
    }
  }
}

