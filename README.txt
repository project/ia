=====================================================
 Invite Aggregator
=====================================================
This module aggregates invite functionality from 
different modules into one place by providing an API
for modules to hook into.

Each module that intends to surface its interface to
ia should implement hook_enable() and hook_disable()
so that pager IDs can be set for that module.  See
modules/ia_user_relationships/ia_user_relationship.module
for an example on how to do this.

=====================================================
 Included Files
=====================================================
README.txt - this file
ia.info - the info file
ia.module - the main module file
modules/ia_invite/ia_invite.info - info for the
  ia_invite module
modules/ia_invite/ia_invite.install - install file
  for the ia_invite module
modules/ia_invite/ia_invite.module - the main module
  file for the ia_invite module
modules/ia_user_relationships/ia_user_relationships.info
   - info for the ia_user relationships module
modules/ia_user_relationships/ia_user_relationships.install
   - install file for the ia_user_relationships module
modules/ia_user_relationships/ia_user_relationships.module
   - the main module file for the ia_user_relationships module

=====================================================
 API
=====================================================
This module provides two API functions so other
moduels can surface their invite interface to the ia module:

hook_ia_links(&$links, &$pages) - add your module's
  links and the pages to display the links on. See
  ia_ia_links() for more detailed documentation.

hook_ia_page(&$invites) - add your module's invites
  to the main ia page so users can interact with all
  surfaced invite interfaces in one place.  See the
  ia_ia_page() function for more detailed documentation 

=====================================================
 Contact
=====================================================
Questions or comments? Email me:
  Elliott Foster
  http://codebrews.com
  elliottf@codebrews.com

