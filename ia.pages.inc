<?php

/**
 * Page callbacks for the ia module.
 *
 * @file: ia.pages.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * ia_page - return the main invite aggregation
 *  page for the current user.
 */
function ia_page() {
  global $user;
  $ias = variable_get('ia_pagers', array());
  $out = '';

  $invites = array();
  ia_invoke('page', $invites);

  foreach ($invites as $module => $m_invites) {
    if (isset($m_invites['sent'])) {
      $out .= '<div class="' . $module . '">' .
        theme('ia_sent_invites', $module, $m_invites['sent_name'], $m_invites['sent']) .
        theme('pager', array(), variable_get('ia_invites_per_page', 10), $ias[$module]['sent']) .
      '</div>' . "\n";
    }
    if (isset($m_invites['received'])) {
      $out .= '<div class="' . $module . '">' .
        theme('ia_received_invites', $module, $m_invites['received_name'], $m_invites['received']) .
        theme('pager', array(), variable_get('ia_invites_per_page', 10), $ias[$module]['received']) .
      '</div>' . "\n";
    }
  }

  $css = drupal_get_path('module', 'ia') . '/theme/ia.css';
  if (file_exists(path_to_theme() . '/ia.css')) {
    $css = path_to_theme() . '/ia.css';
  }
  drupal_add_css($css);

  return '<div class="ia_wrapper">' .
    $out .
  '</div>';
}

